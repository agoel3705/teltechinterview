package com.example.daggerandroidmvvm.lobby.test;

/**
 * Created by agoel on 9/11/2018.
 */

import com.example.daggerandroidmvvm.common.domain.interactors.CallerIdUseCase;
import com.example.daggerandroidmvvm.common.domain.interactors.LoadContactUseCase;
import com.example.daggerandroidmvvm.lobby.ContactViewModel;
import com.example.daggerandroidmvvm.lobby.LoadContactConcreteUseCase;
import com.example.daggerandroidmvvm.rx.SchedulersFacade;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Scheduler;
import io.reactivex.Single;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * Reponsible for Retrieving Contacts of user.
 */
@RunWith(MockitoJUnitRunner.class)
public class ContactViewModelTest {

    @Mock
    LoadContactConcreteUseCase loadContactUseCase;

   @Mock
    CallerIdUseCase callerIdUseCase;

    @Mock
    SchedulersFacade schedulersFacade;

    @Mock
    Scheduler scheduler;

    ContactViewModel contactViewModel;


    @Before
    public void setup() {
        ArrayList<String> contacts = new ArrayList<String>();
        contacts.add("9089022725");
        contacts.add("7322452957");
        Single<List<String>> single = Single.just(contacts);
        contactViewModel = new ContactViewModel(callerIdUseCase, loadContactUseCase, schedulersFacade);

        when(loadContactUseCase.getContacts()).thenReturn(single);
        when(schedulersFacade.io()).thenReturn(scheduler);
        when(schedulersFacade.ui()).thenReturn(scheduler);
    }
    @Test
    public void loadContacts() {

        contactViewModel.loadContacts();

    }

}