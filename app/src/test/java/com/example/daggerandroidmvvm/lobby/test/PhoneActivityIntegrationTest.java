package com.example.daggerandroidmvvm.lobby.test;

import android.Manifest;
import android.provider.ContactsContract;

import com.example.daggerandroidmvvm.BuildConfig;
import com.example.daggerandroidmvvm.lobby.PhoneActivity;

import org.junit.Test;
import org.robolectric.Robolectric;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowApplication;

import static junit.framework.Assert.assertNotNull;

/**
 * Created by agoel on 9/11/2018.
 */


@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class , sdk=21)
public class PhoneActivityIntegrationTest {

    private PhoneActivity phoneActivity;

    private ShadowApplication application;

    @Before
    public void setUp() throws Exception {
        application =  new ShadowApplication();
        application.grantPermissions( new String[]{Manifest.permission
                .ANSWER_PHONE_CALLS, Manifest.permission.CALL_PHONE, Manifest.permission.READ_CONTACTS, Manifest.permission.READ_PHONE_STATE, Manifest.permission.READ_CALL_LOG});
        phoneActivity = Robolectric.buildActivity(PhoneActivity.class).create().resume().get();


    }


    @Test
    public void shouldNotBeNull() throws Exception  {
        assertNotNull(phoneActivity);
    }
}
