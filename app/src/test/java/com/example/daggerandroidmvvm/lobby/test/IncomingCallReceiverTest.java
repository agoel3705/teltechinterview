package com.example.daggerandroidmvvm.lobby.test;

import android.app.Instrumentation;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.test.RenamingDelegatingContext;

import com.example.daggerandroidmvvm.service.IncomingCallReceiver;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

/**
 * Created by agoel on 9/11/2018.
 */

@RunWith(MockitoJUnitRunner.class)
public class IncomingCallReceiverTest {

    @Mock
    Context context;


    @Mock
    Intent intent;

    @Mock
    Bundle bundle;
    IncomingCallReceiver receiver;
    ArgumentCaptor<Context> captureContext = ArgumentCaptor.forClass(Context.class);


    @Before
    public void init() {
        receiver = new IncomingCallReceiver();
        when(intent.getStringExtra(TelephonyManager.EXTRA_STATE)).thenReturn(TelephonyManager.EXTRA_STATE_OFFHOOK);
        when(intent.getExtras()).thenReturn(bundle);
        when(bundle.getString(TelephonyManager.EXTRA_INCOMING_NUMBER)).thenReturn("9089022725");

    }


    @Test
    public void onReceive() {
        receiver.onReceive(context, intent);
    }

}
