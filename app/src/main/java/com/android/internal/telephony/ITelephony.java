package com.android.internal.telephony;

/**
 * Created by agoel on 9/11/2018.
 */

public interface ITelephony {
    boolean endCall();
    void answerRingingCall();
    void silenceRinger();
}