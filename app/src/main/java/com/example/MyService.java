package com.example;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.telecom.Connection;
import android.telecom.ConnectionRequest;
import android.telecom.ConnectionService;
import android.telecom.PhoneAccountHandle;
import android.util.Log;

/**
 * Created by agoel on 9/11/2018.
 */

@RequiresApi(api = Build.VERSION_CODES.M)
public class MyService extends ConnectionService {

    @Override
    public Connection onCreateIncomingConnection(PhoneAccountHandle connectionManagerPhoneAccount, ConnectionRequest request) {
        Log.d("Ankit" ,  "Inside  COnnection s ervice");
        return super.onCreateIncomingConnection(connectionManagerPhoneAccount, request);
    }
}
