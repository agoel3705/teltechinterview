package com.example.daggerandroidmvvm.lobby;

import android.content.Context;

import com.example.daggerandroidmvvm.App;
import com.example.daggerandroidmvvm.common.domain.interactors.CallerIdUseCase;
import com.example.daggerandroidmvvm.rx.SchedulersFacade;

import dagger.Module;
import dagger.Provides;

/**
 * Define PhoneActivity-specific dependencies here.
 */
@Module
public class ContactModule {


    @Provides
    Context provideContext(App application) {
        return application.getApplicationContext();
    }

    @Provides
    ContactRepository provideLobbyGreetingRepository(App application) {
        return new ContactRepository(application);
    }

    @Provides
    LobbyViewModelFactory provideLobbyViewModelFactory(CallerIdUseCase loadCommonGreetingUseCase,
                                         LoadContactConcreteUseCase loadLobbyGreetingUseCase,
                                         SchedulersFacade schedulersFacade) {
        return new LobbyViewModelFactory(loadCommonGreetingUseCase, loadLobbyGreetingUseCase, schedulersFacade);
    }
}
