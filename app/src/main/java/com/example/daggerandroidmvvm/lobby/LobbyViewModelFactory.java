package com.example.daggerandroidmvvm.lobby;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.example.daggerandroidmvvm.common.domain.interactors.CallerIdUseCase;
import com.example.daggerandroidmvvm.rx.SchedulersFacade;

class LobbyViewModelFactory implements ViewModelProvider.Factory {

    private final CallerIdUseCase loadCommonGreetingUseCase;

    private final LoadContactConcreteUseCase loadLobbyGreetingUseCase;

    private final SchedulersFacade schedulersFacade;

    LobbyViewModelFactory(CallerIdUseCase loadCommonGreetingUseCase,
                          LoadContactConcreteUseCase loadLobbyGreetingUseCase,
                          SchedulersFacade schedulersFacade) {
        this.loadCommonGreetingUseCase = loadCommonGreetingUseCase;
        this.loadLobbyGreetingUseCase = loadLobbyGreetingUseCase;
        this.schedulersFacade = schedulersFacade;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        if (modelClass.isAssignableFrom(ContactViewModel.class)) {
            return (T) new ContactViewModel(loadCommonGreetingUseCase, loadLobbyGreetingUseCase, schedulersFacade);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
