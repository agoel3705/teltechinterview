package com.example.daggerandroidmvvm.lobby;

import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

public class ContactRepository {
    Context context;

    public ContactRepository(Context context) {
        this.context = context;
    }

    Single<String> getGreeting() {
        return Single.just("Hello from ContactRepository");

    }

    public Single<List<String>> getContacts() {


        ArrayList<String> contactNumbers = new ArrayList<String>();
        Cursor phones = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
        while (phones.moveToNext()) {
            String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            phoneNumber = phoneNumber.replaceAll("\\s+","");
            phoneNumber = phoneNumber.replaceAll("[^\\d.]", "");

            contactNumbers.add(phoneNumber);

        }
        phones.close();

        ArrayList<String> cont = new ArrayList<String>();
        return Single.just(contactNumbers);

    }

}
