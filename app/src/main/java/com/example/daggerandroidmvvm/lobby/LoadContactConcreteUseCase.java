package com.example.daggerandroidmvvm.lobby;

import com.example.daggerandroidmvvm.common.domain.interactors.LoadContactUseCase;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

public class LoadContactConcreteUseCase implements LoadContactUseCase {
    private final ContactRepository contactsRepository;

    @Inject
    public LoadContactConcreteUseCase(ContactRepository contactsRepository) {
        this.contactsRepository = contactsRepository;
    }

    @Override
    public Single<String> execute() {
        return contactsRepository.getGreeting();
    }

    @Override
    public Single<List<String>> getContacts() {
        return contactsRepository.getContacts();
    }
}
