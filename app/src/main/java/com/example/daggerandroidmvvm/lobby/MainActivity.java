package com.example.daggerandroidmvvm.lobby;

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.provider.BlockedNumberContract;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telecom.PhoneAccount;
import android.telecom.PhoneAccountHandle;
import android.telecom.TelecomManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.example.daggerandroidmvvm.R;
import com.example.daggerandroidmvvm.common.domain.interactors.SmartTileView;
import com.example.daggerandroidmvvm.network.model.ApiClient;
import com.example.daggerandroidmvvm.network.model.ApiService;
import com.example.daggerandroidmvvm.network.model.Note;
import com.example.daggerandroidmvvm.network.model.User;
import com.example.daggerandroidmvvm.service.MyConnectionService;
import com.example.daggerandroidmvvm.utils.MyDividerItemDecoration;
import com.example.daggerandroidmvvm.utils.PrefUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.AndroidInjection;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity {

    private ApiService apiService;

    private ArrayList<Note> notesList = new  ArrayList<Note>();

    private NotesAdapter mAdapter;

    @BindView(R.id.recycler_view)
    public RecyclerView recyclerView;

    private TelephonyManager tm;

    @Inject
    Retrofit service;

    private static final String TAG = "MainActivity";

    private CompositeDisposable disposable = new CompositeDisposable();

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        AndroidInjection.inject(this);
        ButterKnife.bind(this);
        apiService = service.create(ApiService.class);
//        SmartTileView tileView = new SmartTileView(this);
//        FrameLayout f1 = findViewById(R.id.f1);
//        //f1.addView(tileView);

        String uniqueId = UUID.randomUUID().toString();
        mAdapter = new NotesAdapter(this, notesList);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.VERTICAL, 16));
        recyclerView.setAdapter(mAdapter);

        Intent intent = new Intent(TelecomManager.ACTION_CHANGE_DEFAULT_DIALER);
        intent.putExtra(TelecomManager.EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME,
                this.getPackageName());
        startActivity(intent);

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ANSWER_PHONE_CALLS )
                != PackageManager.PERMISSION_GRANTED) {



            requestPermissions(
                    new String[]{Manifest.permission
                            .ANSWER_PHONE_CALLS,  Manifest.permission.CALL_PHONE  , Manifest.permission.READ_PHONE_STATE , Manifest.permission.READ_CALL_LOG},
                    1);







        }

        if(ContextCompat.checkSelfPermission(this ,  Manifest.permission.BIND_TELECOM_CONNECTION_SERVICE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[] { Manifest.permission.BIND_TELECOM_CONNECTION_SERVICE} , 2);
        }
//        CallStateListener callStateListener = new CallStateListener();
//        tm = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
//        tm.listen(callStateListener, PhoneStateListener.LISTEN_CALL_STATE);

        TelecomManager telecomManager = (TelecomManager) getSystemService(TELECOM_SERVICE);

        boolean isAlreadyDefaultDialer = getPackageName()
                .equals(telecomManager.getDefaultDialerPackage());

        if (isAlreadyDefaultDialer) {
            return;
        }

        Cursor c = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            c = getContentResolver().query(BlockedNumberContract.BlockedNumbers.CONTENT_URI,
                    new String[]{BlockedNumberContract.BlockedNumbers.COLUMN_ID,
                            BlockedNumberContract.BlockedNumbers.COLUMN_ORIGINAL_NUMBER,
                            BlockedNumberContract.BlockedNumbers.COLUMN_E164_NUMBER}, null, null, null);
        }
        while(c.moveToFirst()) {
            String cd = c.getString(c.getColumnIndex(BlockedNumberContract.BlockedNumbers.COLUMN_ORIGINAL_NUMBER));
            Log.d("Ankit" , cd);
        }

        disposable.add(
                apiService.fetchAllNotes()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .map(new Function<List<Note>, List<Note>>() {
                            @Override
                            public List<Note> apply(List<Note> notes) throws Exception {
                                // TODO - note about sort
                                Collections.sort(notes, new Comparator<Note>() {
                                    @Override
                                    public int compare(Note n1, Note n2) {
                                        return n2.getId() - n1.getId();
                                    }
                                });
                                return notes;
                            }
                        })
                        .subscribeWith(new DisposableSingleObserver<List<Note>>() {
                            @Override
                            public void onSuccess(List<Note> notes) {
                                notesList.clear();
                                notesList.addAll(notes);
                                mAdapter.notifyDataSetChanged();

                            }

                            @Override
                            public void onError(Throwable e) {
                                Log.e(TAG, "onError: " + e.getMessage());
                            }
                        })
        );


//        disposable.add(
//                apiService
//                        .register(uniqueId)
//                        .subscribeOn(Schedulers.io())
//                        .observeOn(AndroidSchedulers.mainThread())
//                        .subscribeWith(new DisposableSingleObserver<User>() {
//                            @Override
//                            public void onSuccess(User user) {
//                                // Storing user API Key in preferences
//                                PrefUtils.storeApiKey(getApplicationContext(), user.getApiKey());
//
//                                Toast.makeText(getApplicationContext(),
//                                        "Device is registered successfully! ApiKey: " + PrefUtils.getApiKey(getApplicationContext()),
//                                        Toast.LENGTH_LONG).show();
//                            }
//
//                            @Override
//                            public void onError(Throwable e) {
//                                Log.e(TAG, "onError: " + e.getMessage());
//
//                            }
//                        }));
    }


//    private class CallStateListener extends PhoneStateListener {
//        @Override
//        public void onCallStateChanged(int state, String incomingNumber) {
//            switch (state) {
//                case TelephonyManager.CALL_STATE_RINGING:
//                    // called when someone is ringing to this phone
//
//                   Log.d("Ankit" ,  "Ankit");
//                    break;
//            }
//        }
//    }
}

