package com.example.daggerandroidmvvm.lobby;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.example.daggerandroidmvvm.common.domain.interactors.CallerIdUseCase;
import com.example.daggerandroidmvvm.common.domain.interactors.LoadContactUseCase;
import com.example.daggerandroidmvvm.common.viewmodel.Response;
import com.example.daggerandroidmvvm.network.model.ContactRepo;
import com.example.daggerandroidmvvm.rx.SchedulersFacade;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;

public class ContactViewModel extends ViewModel {

    private final CallerIdUseCase loadCommonGreetingUseCase;

    private final LoadContactConcreteUseCase loadLobbyGreetingUseCase;

    private final SchedulersFacade schedulersFacade;

    private ArrayList<String> phoneNumbers = new ArrayList<String>();

    private final CompositeDisposable disposables = new CompositeDisposable();

    private final MutableLiveData<Response> response = new MutableLiveData<>();

    public ContactViewModel(CallerIdUseCase loadCommonGreetingUseCase,
                     LoadContactConcreteUseCase loadLobbyGreetingUseCase,
                     SchedulersFacade schedulersFacade) {
        this.loadCommonGreetingUseCase = loadCommonGreetingUseCase;
        this.loadLobbyGreetingUseCase = loadLobbyGreetingUseCase;
        this.schedulersFacade = schedulersFacade;
    }

    @Override
    protected void onCleared() {
        disposables.clear();
    }

    void loadCommonGreeting() {
        loadGreeting(loadCommonGreetingUseCase);
    }



  public  void loadContacts() {
        loadContacts(loadLobbyGreetingUseCase);
    }

    MutableLiveData<Response> response() {
        return response;
    }

    private void loadGreeting(LoadContactUseCase loadContactUseCase) {
        disposables.add(loadContactUseCase.execute()
                .subscribeOn(schedulersFacade.io())
                .observeOn(schedulersFacade.ui())
                .doOnSubscribe(__-> response.setValue(Response.loading()))
                .subscribe(
                        greeting -> response.setValue(Response.success(greeting)),
                        throwable -> response.setValue(Response.error(throwable))
                )
        );
    }

    private void loadContacts(LoadContactUseCase loadContactUseCase) {
        disposables.add(
                loadContactUseCase.getContacts()
                        .subscribeOn(schedulersFacade.io())
                        .observeOn(schedulersFacade.ui())

                        .subscribeWith(new DisposableSingleObserver<List<String>>() {
                            @Override
                            public void onSuccess(List<String> notes) {

                                ContactRepo rep = ContactRepo.initialize();
                                rep.setContacts((ArrayList<String>)notes);

                            }

                            @Override
                            public void onError(Throwable e) {
                            }
                        })
        );
    }




}
