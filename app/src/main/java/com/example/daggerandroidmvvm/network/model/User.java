package com.example.daggerandroidmvvm.network.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by agoel on 8/23/2018.
 */

public class User extends BaseResponse {

    @SerializedName("api_key")
    String apiKey;

    public String getApiKey() {
        return apiKey;
    }
}
