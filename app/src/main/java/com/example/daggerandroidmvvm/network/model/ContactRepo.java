package com.example.daggerandroidmvvm.network.model;

import java.util.ArrayList;

/**
 * Created by agoel on 9/11/2018.
 */

public class ContactRepo {


    public static ArrayList<String> getContacts() {
        return contacts;
    }

    public static void setContacts(ArrayList<String> contacts) {
        ContactRepo.contacts = contacts;
    }

    private static ArrayList<String> contacts = new ArrayList<String>();

    private static ContactRepo instance;
    public static ContactRepo initialize() {
        if(instance ==  null) {
            instance  = new ContactRepo();
        }

        return instance;
    }
    private ContactRepo() {

    }
}
