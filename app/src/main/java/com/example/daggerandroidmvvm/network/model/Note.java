package com.example.daggerandroidmvvm.network.model;

/**
 * Created by agoel on 8/23/2018.
 */

public class Note extends BaseResponse {
    int id;
    String note;
    String timeStamp;

    public int getId() {
        return id;
    }

    public String getNote(){
        return note;
    }

    public void setNote(String  note) {
        this.note = note;
    }

    public String getTimeStamp() {
        return timeStamp;
    }
}
