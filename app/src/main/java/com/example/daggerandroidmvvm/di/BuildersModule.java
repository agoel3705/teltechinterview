package com.example.daggerandroidmvvm.di;

import com.example.daggerandroidmvvm.lobby.PhoneActivity;
import com.example.daggerandroidmvvm.lobby.ContactModule;
import com.example.daggerandroidmvvm.lobby.MainActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Binds all sub-components within the app.
 */
@Module
public abstract class BuildersModule {

    @ContributesAndroidInjector(modules = ContactModule.class)
    abstract PhoneActivity bindLobbyActivity();


    @ContributesAndroidInjector()
    abstract MainActivity bindMainActivity();

    // Add bindings for other sub-components here
}
