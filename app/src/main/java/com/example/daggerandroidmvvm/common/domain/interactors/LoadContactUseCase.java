package com.example.daggerandroidmvvm.common.domain.interactors;

import com.example.daggerandroidmvvm.network.model.Note;

import java.util.List;

import io.reactivex.Single;

public interface LoadContactUseCase {
    Single<String> execute();

    Single<List<String>> getContacts();


}
