package com.example.daggerandroidmvvm.common.domain.interactors;

import com.example.daggerandroidmvvm.common.domain.model.CommonGreetingRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

public class CallerIdUseCase implements LoadContactUseCase {
    private final CommonGreetingRepository greetingRepository;

    @Inject
    public CallerIdUseCase(CommonGreetingRepository greetingRepository) {
        this.greetingRepository = greetingRepository;
    }

    @Override
    public Single<String> execute() {
        return greetingRepository.getGreeting();
    }

    @Override
    public Single<List<String>> getContacts() {
        return null;
    }
}
