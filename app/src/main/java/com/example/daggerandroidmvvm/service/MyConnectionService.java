package com.example.daggerandroidmvvm.service;

import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.RequiresApi;
import android.telecom.Connection;
import android.telecom.ConnectionRequest;
import android.telecom.ConnectionService;
import android.telecom.InCallService;
import android.telecom.PhoneAccountHandle;
import android.util.Log;

/**
 * Created by agoel on 9/6/2018.
 */

@RequiresApi(api = Build.VERSION_CODES.M)
public class MyConnectionService extends InCallService {


    @Override
    public IBinder onBind(Intent intent) {
        Log.d("Call Added" ,  "This call was added");

        return super.onBind(intent);
    }
}
