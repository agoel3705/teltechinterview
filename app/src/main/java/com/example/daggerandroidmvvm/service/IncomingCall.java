package com.example.daggerandroidmvvm.service;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.telecom.Connection;
import android.util.Log;

/**
 * Created by agoel on 9/6/2018.
 */

@RequiresApi(api = Build.VERSION_CODES.M)
public class IncomingCall extends Connection {

    @Override
    public void onShowIncomingCallUi() {
        Log.d("Incoming Call" , "Incoming cALL");
        super.onShowIncomingCallUi();
    }
}
