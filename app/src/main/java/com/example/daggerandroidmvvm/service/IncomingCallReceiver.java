package com.example.daggerandroidmvvm.service;

/**
 * Created by agoel on 9/11/2018.
 */

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.BlockedNumberContract;
import android.support.annotation.RequiresApi;
import android.telecom.PhoneAccount;
import android.telecom.PhoneAccountHandle;
import android.telecom.TelecomManager;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import java.lang.reflect.Method;

import com.android.internal.telephony.ITelephony;
import com.example.MyService;
import com.example.daggerandroidmvvm.App;
import com.example.daggerandroidmvvm.R;
import com.example.daggerandroidmvvm.network.model.ContactRepo;

public class IncomingCallReceiver extends BroadcastReceiver {
    @SuppressLint("NewApi")
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onReceive(Context context, Intent intent) {


        ITelephony telephonyService;

        try {
            String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
            String number = intent.getExtras().getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
            ContactRepo repo = ContactRepo.initialize();




            // Dangerous Calls
            if (number.equalsIgnoreCase(context.getResources().getString(R.string.blockedCall))) {
                if ((number != null)) {
                    // Block  Number

                    TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                    try {
                        Method m = tm.getClass().getDeclaredMethod("getITelephony");

                        m.setAccessible(true);
                        telephonyService = (ITelephony) m.invoke(tm);

                        if ((number != null)) {
                            telephonyService.endCall();
                            Toast.makeText(context, "Ending the call from: " + number, Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }
                //  Not in contact List show popup
                if (!(repo.getContacts().contains("1" + number) || repo.getContacts().contains(number))) {
                    Toast.makeText(context, "Suspicious Call " + number, Toast.LENGTH_SHORT).show();
                }


                if (state.equalsIgnoreCase(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
                    Toast.makeText(context, "Answered " + number, Toast.LENGTH_SHORT).show();
                }
                if (state.equalsIgnoreCase(TelephonyManager.EXTRA_STATE_IDLE)) {
                    Toast.makeText(context, "Idle " + number, Toast.LENGTH_SHORT).show();
                }
            } catch(Exception e){
                e.printStackTrace();
            }
        }
    }