package com.example.daggerandroidmvvm.sqlite.database.model;

/**
 * Created by agoel on 9/4/2018.
 */

public class Note {

    public static final String TABLE_NAME = "notes";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NOTE = "note";
    public static final String COLUMN_TIMESTAMP = "timestamp";
    public static final String CREATE_TABLE = "CREATE TABLE" + TABLE_NAME + "(" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ," + COLUMN_NOTE + " TEXT," + COLUMN_TIMESTAMP + " DATETIME DEFAULT CURRENT_TIMESTAMP" + ")";

    private int id;
    private String note;
    private String timestamp;


    public Note() {

    }

    public Note(int id , String note , String timestamp) {
        this.id = id;
        this.note = note;
        this.timestamp = timestamp;
    }


    public void setNote(String note) {
        this.note = note;
    }



    // Create Table SQL query

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }


    public int getId() {
        return id;
    }

    public String getNote() {
            return note;
    }


}
