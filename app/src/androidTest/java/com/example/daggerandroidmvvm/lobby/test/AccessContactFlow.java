package com.example.daggerandroidmvvm.lobby.test;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.example.daggerandroidmvvm.R;
import com.example.daggerandroidmvvm.lobby.PhoneActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by agoel on 9/11/2018.
 */

@RunWith(AndroidJUnit4.class)
public class AccessContactFlow {

    @Rule
    public ActivityTestRule<PhoneActivity> mActivityRule = new ActivityTestRule<PhoneActivity>(PhoneActivity.class);

    /**
     * This is just to show that my Phone Activity successfully retrieves list of contacts. Meaning  UI is functional
     */
    @Test
    public void retrieveContacts() {
        onView(withId(R.id.lobby_greeting_button)).perform(click());

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
